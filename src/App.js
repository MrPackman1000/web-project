import React, { Component } from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Route, BrowserRouter } from 'react-router-dom';
import Home from './Pages/Home.js';
import About from './Pages/About.js';
import Cities from './Pages/Cities.js';
import Navigation from './Components/Navigation.js';
import Universities from './Pages/Universities.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Route exact path="/" component={Home} />
          <Route path={'/universities'} component={Universities} />
          <Route path={'/about'} component={About} />
          <Route path={'/home'} component={Home} />
          <Route path={'/cities'} component={Cities} />
        </BrowserRouter>
      </div>
    );
  }
}
export default App;
