import React, { Component } from 'react';
import '../Styles/Home.css';
import Navigation from '../Components/Navigation';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import Footer from '../Components/Footer';
import Cities from '../static_resources/cities.png';
import Universities from '../static_resources/universities.png';
import Restaurants from '../static_resources/restaurants.png';

class Home extends Component {
  render() {
    return (
      <div>
        <Navigation />
        <Jumbotron>
          <h1 class="text-left">Hello Students!</h1>
          <p class="text-left">
            Find the University that's the best fit for your interests and
            budget.
            <br />
            Learn more different Universities, their surrounding areas, and
            attractions near them.
          </p>
          <p class="text-left">
            <Button variant="primary">Explore</Button>
          </p>
        </Jumbotron>

        <CardDeck>
          <Card>
            <Card.Img variant="top" height="240" src={Cities} />
            <Card.Body>
              <Card.Title>Cities</Card.Title>
              <Card.Text>See the best cities for college students</Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" height="240" src={Universities} />
            <Card.Body>
              <Card.Title>Universities</Card.Title>
              <Card.Text>Pick the University that is best for you</Card.Text>
            </Card.Body>
          </Card>
          <Card>
            <Card.Img variant="top" height="240" src={Restaurants} />
            <Card.Body>
              <Card.Title>Restauraunts</Card.Title>
              <Card.Text>
                Explore restaurants and attractions in the area
              </Card.Text>
            </Card.Body>
          </Card>
        </CardDeck>
        <p></p>
        <Footer />
      </div>
    );
  }
}

export default Home;
