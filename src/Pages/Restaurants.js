import React, { Component } from 'react';
import '../Styles/Home.css';
import Navigation from '../Components/Navigation';
import Footer from '../Components/Footer';

class Restaurants extends Component {
  render() {
    return (
      <div>
        <Navigation />

        <Footer />
      </div>
    );
  }
}

export default Restaurants;