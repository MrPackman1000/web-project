import React, { Component } from 'react';
import '../Styles/Home.css';
import Navigation from '../Components/Navigation';
import Footer from '../Components/Footer';
import Gitlab from '../Components/Gitlab'

class About extends Component {
  render() {
    return (
      <div>
        <Navigation />
        <Gitlab />
        <Footer />
      </div>
    );
  }
}

export default About;
