import React, { Component } from 'react';
import '../Styles/Cities.css';
import Navigation from '../Components/Navigation';
import Footer from '../Components/Footer';
import Jumbotron from 'react-bootstrap/Jumbotron';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Cities extends Component {
  render() {
    return (
      <div>
        <Navigation />
        <Jumbotron className="cities-jumbotron"></Jumbotron>

        <p>
          City matters a lot when deciding a University to attend. <br />
          Whether you're a West Coast or East Coast person, prefer a big or
          small city, or looking for a city that has great access to nature,
          there's a city for you!
        </p>

        <Container>
          <Row>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="cities-card">
                <Card.Img variant="top" height="200" />
                <Card.Body>
                  <Card.Title>City Name</Card.Title>
                  <Card.Text>Student Score: XX.XX</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
        <p></p>
        <Footer />
      </div>
    );
  }
}

export default Cities;
