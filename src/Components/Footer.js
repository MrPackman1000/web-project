import React from 'react';
import '../Styles/Footer.css';

class Footer extends React.Component {
  render() {
    return (
      <body>
        <div className="footer">
          <h5> Find Your College with CollegeFitFor.me </h5>
        </div>
      </body>
    );
  }
}

export default Footer;
