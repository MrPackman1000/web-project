import React from 'react';
import { Nav, Navbar, Form, FormControl, Button } from 'react-bootstrap';
// import '../Styles/Navigation.css';

class Navigation extends React.Component {
  render() {
    return (
      <>
        <Navbar bg="light" variant="light">
          <Navbar.Brand href="/home">College Fit For Me</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="/cities">Cities</Nav.Link>
            <Nav.Link href="/universities">Universities</Nav.Link>
            <Nav.Link href="/restauraunts">Restauraunts</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/api">API</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-primary">Search</Button>
          </Form>
        </Navbar>
      </>
    );
  }
}

export default Navigation;
