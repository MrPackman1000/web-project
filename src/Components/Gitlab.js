import React from 'react'
import Table from 'react-bootstrap/Table'

class Gitlab extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            commits: [],
            issues: []
        };
    }

    componentDidMount() {
        fetch('https://gitlab.com/api/v4/projects/16968914/repository/commits?per_page=1000&page=1')
        .then(res => res.json())
        .then((data) => {
          this.setState({commits : data});
          console.log(this.state.commits)
        })
        .catch(console.log)


        fetch('https://gitlab.com/api/v4/projects/16968914/repository/issues?per_page=1000&page=1')
        .then(res => res.json())
        .then((data) => {
          this.setState({issues : data});
        //   console.log(this.state.commits)
        })
        .catch(console.log)
    }

    

    render() {

       var commitMap = {
            "sparth99@gmail.com": 0,
            "leoncai197@gmail.com": 0,
            "mmunoz11@cs.utexas.edu": 0,
            "Total": 0
       };

     

       var commit;

       for(commit of this.state.commits){
           if(commit.author_email === 'sparth99@gmail.com'){
               commitMap['sparth99@gmail.com'] += 1;
           }
           if(commit.author_email === 'leoncai197@gmail.com'){
               commitMap['leoncai197@gmail.com'] += 1;
           }
           if(commit.author_email === 'mmunoz11@cs.utexas.edu'){
            commitMap['mmunoz11@cs.utexas.edu'] += 1;
           }

           commitMap['Total'] += 1;
       }


       console.log("Hi");
       console.log(this.state.commits);

      


      return (
        <body>
            <Table striped bordered hover>
            <thead>
                <tr>
                <th>Name</th>
                <th>Commits</th>
                <th>Issues</th>
                <th>Tests</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>Parth Shah</td>
                <td>{commitMap['sparth99@gmail.com']}</td>
                <td>Otto</td>
                <td>@mdo</td>
                </tr>
                <tr>
                <td>Alec Hilyard</td>
                <td>{commitMap['sparth99@gmail.com']}</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <td>Chinedu Enwere</td>
                <td>{commitMap['sparth99@gmail.com']}</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <td>Leon Cai</td>
                <td>{commitMap['leoncai197@gmail.com']}</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <td>Michael Munoz</td>
                <td>{commitMap['mmunoz11@cs.utexas.edu']}</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <td>Jed Eloja</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <td>Total</td>
                <td>{commitMap['mmunoz11@cs.utexas.edu']}</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
            </tbody>
            </Table>
        </body>
      );
    }
  }
  

export default Gitlab